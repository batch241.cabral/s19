let username;
let password;
let role;

function logInUser() {
    username = prompt("Enter your username:");
    password = prompt("Enter your password:");
    role = prompt("Enter your role:");

    if (
        (username = null || username === "") &&
        (password = null || password === "") &&
        (role = null || role === "")
    ) {
        alert("User input should not be empty");
    } else {
        switch (role) {
            case "admin":
                alert("Welcome back to the class portal, admin!");
                break;
            case "teacher":
                alert("Thank you for logging in, teacher!");
                break;
            case "student":
                alert("Welcome to the class portal, student!");
                break;
            default:
                alert("Role out of range.");
                break;
        }
    }
}
logInUser();

function getAverage(num1, num2, num3, num4) {
    let average = (num1 + num2 + num3 + num4) / 4;
    average = Math.round(average);
    if (average <= 74) {
        console.log(
            "Hello, student, your average is " +
                average +
                ". The letter equivalent is F"
        );
    } else if (average > 74 && average <= 79) {
        console.log(
            "Hello, student, your average is " +
                average +
                ". The letter equivalent is D"
        );
    } else if (average > 79 && average <= 84) {
        console.log(
            "Hello, student, your average is " +
                average +
                ". The letter equivalent is C"
        );
    } else if (average > 84 && average <= 89) {
        console.log(
            "Hello, student, your average is " +
                average +
                ". The letter equivalent is B"
        );
    } else if (average > 89 && average <= 95) {
        console.log(
            "Hello, student, your average is " +
                average +
                ". The letter equivalent is A"
        );
    } else if (average > 95) {
        console.log(
            "Hello, student, your average is " +
                average +
                ". The letter equivalent is A+"
        );
    }
}

console.log(getAverage());

// Just copy paste this on the browser console one by one to get the expected results
// getAverage(71, 70, 73, 74);
// getAverage(75, 75, 76, 78);
// getAverage(80, 81, 82, 78);
// getAverage(84, 85, 87, 88);
// getAverage(89, 90, 91, 90);
// getAverage(91, 96, 97, 95);
